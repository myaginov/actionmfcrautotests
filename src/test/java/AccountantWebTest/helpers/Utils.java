package AccountantWebTest.helpers;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

/**
 *  Класс для служебных методов
 */
public class Utils {

    /**
     * Метод для удаления фокуса с элементов страницы
     * @param driver - Драйвер браузера
     */
    public static void unfocusElement(WebDriver driver) {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript(
                "var tmp = document.createElement(\"input\");\n" +
                "document.body.appendChild(tmp);\n" +
                "tmp.focus();\n" +
                "document.body.removeChild(tmp);");
    }

}
