package AccountantWebTest;

import AccountantWebTest.pages.MainPage;
import AccountantWebTest.pages.LawBasePage;
import AccountantWebTest.pages.AuthPage;
import AccountantWebTest.pages.SearchResultPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import java.io.IOException;



public class SearchTestSuite {

    private WebDriver driver;

    /**
     * Запуск и инициализация браузера
     * @param url - адресс главной страницы
     */
    @BeforeTest
    @Parameters({"url"})
    public void setup(String url) {
        System.setProperty("webdriver.chrome.driver", "libs/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(url);
        driver.manage().window().maximize();
    }

    /**
     *  Тест-кейс для проверки функции поиска
     * @param userLogin - логин
     * @param userPassword - пароль
     */
    @Test
    @Parameters({"userLogin", "userPassword"})
    public void searchTest(String userLogin, String userPassword) {
        String searchText = "налог";
        String decree = "Постановление";
        String region = "Москва";
        String expectedCaption = "Результаты поиска по реквизитам: «"+searchText+"»";
        String expectedSearchLists = "Вид — «"+decree+"», регион — «"+region+"»";

        MainPage mainPage = new MainPage(driver);
        AuthPage authPage = mainPage.toAuthPage();
        mainPage = authPage.login(userLogin,userPassword);
        LawBasePage lawBasePage = mainPage.toLawBasePage();
        SearchResultPage searchResultPage = lawBasePage.extendedSearch(searchText, decree, region);

        Assert.assertTrue(searchResultPage.correctResults(expectedCaption,expectedSearchLists));
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}
