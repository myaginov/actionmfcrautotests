package AccountantWebTest.pages;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Класс для страницы результатов поиска
 */
public class SearchResultPage {

    private WebDriver driver;

    @FindBy(xpath = "//ul[@data-filtr-name='pubDiv']")
    private WebElement linkLawBase;

    @FindBy(xpath = "//div[@class='search-extended-string'][1]")
    private WebElement typeListAndRegionList;

    @FindBy(xpath = "//div[@class='search-result'][1]/h1")
    private WebElement caption;

    @FindBy(id = "searchResultsSection")
    private WebElement resultSection;

    public SearchResultPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(resultSection));
    }

    private boolean keyElementsDisplayed(){
        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(resultSection));
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(caption));
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(typeListAndRegionList));
            new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(linkLawBase));
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }

    /**
     * Проверка отображения ключевых элементов на странице, соответствия текста заголовка и соответствия выбранных значений вида,региона.
     * @param expectedCaption - ожидаемый заголовок
     * @param expectedSearchLists - ожидаемый вид и регион
     * @return - true, если все элементы видны, ожидаемый текст заголовка соотвествует действительному и ожидаемые
     * значения вида и региона соответствуют выбранным, false, если хотябы один из элементов не отображается на
     * странице, или ожидаемый текст заголовка не соотвествует действительному, или ожидаемые
     * значения вида и региона не соответствуют выбранным
     * @throws TimeoutException
     */
    public boolean correctResults(String expectedCaption, String expectedSearchLists) throws TimeoutException {
        if (keyElementsDisplayed()) {
            if (expectedCaption.equals(caption.getText())
                    && expectedSearchLists.equals(typeListAndRegionList.getText())) {
                return true;
            }
        }
        return false;
    }
}
