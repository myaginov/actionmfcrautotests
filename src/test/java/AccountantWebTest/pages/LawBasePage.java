package AccountantWebTest.pages;

import AccountantWebTest.helpers.Utils;
import javafx.scene.control.Tab;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Класс для страницы "Правовая база"
 */
public class LawBasePage {

    private WebDriver driver;

    @FindBy(id = "search-button-extended")
    private WebElement buttonSearchExtends;

    @FindBy(id = "search-form-extended")
    private WebElement formSearchExtends;

    @FindBy(id = "search-text")
    private WebElement inputSearchText;

    private String typeListLocatorString = "//*[@id='typelist']/li[text()='%s']";
    private String typeRegionLocatorString = "//*[@id='regionlist']/li[text()='%s']";

    @FindBy(id = "button-search-extended")
    private WebElement buttonSearch;

    private String pageTitle = "Правовая база - Система Главбух. Версия для коммерческих организаций";

    private void clickOnButtonSearchExtends(){
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf( buttonSearchExtends));
        buttonSearchExtends.click();
    }

    private void setSearchData(String searchText, String typeListValue, String typeRegionValue){
        By typeListLocator = By.xpath(String.format(typeListLocatorString, typeListValue));
        By typeRegionLocator = By.xpath(String.format(typeRegionLocatorString, typeRegionValue));
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(formSearchExtends));
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(inputSearchText));
        inputSearchText.sendKeys(searchText);
        Utils.unfocusElement(driver);
        WebElement typeList = driver.findElement(typeListLocator);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(typeList));
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(typeList));
        typeList.click();
        WebElement typeRegion = driver.findElement(typeRegionLocator);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(typeRegion));
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(typeRegion));
        typeRegion.click();
    }

    private SearchResultPage clickOnButtonSearch() {
        buttonSearch.click();
        return new SearchResultPage(driver);
    }


    public LawBasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, 10).until(ExpectedConditions.titleIs(pageTitle));
    }

    /**
     * Поиск введеного текста с выбором вида документа и региона
     * @param searchText - текст поиска
     * @param typeList  - вид документа
     * @param typeRegion - регион
     * @return - страница результата поиска
     */
    public SearchResultPage extendedSearch(String searchText, String typeList, String typeRegion){
        clickOnButtonSearchExtends();
        setSearchData(searchText, typeList, typeRegion);
        return clickOnButtonSearch();
    }
}
