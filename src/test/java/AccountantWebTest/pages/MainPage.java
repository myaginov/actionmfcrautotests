package AccountantWebTest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Класс для главной страницы
 */
public class MainPage {

    private WebDriver driver;

    @FindBy(id = "user-enter")  //кнопка "Вход и регистрация"
    private WebElement  buttonLoginAndRegistration;

    @FindBy(css = "a.btn_content_law")  //кнопка "Правовая база"
    private WebElement  buttonLawBase;

    private String pageTitle = "Система Главбух. Версия для коммерческих организаций";

    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, 10).until(ExpectedConditions.titleIs(pageTitle));
    }

    /**
     * Переход на страницу авторизации
     * @return - страница авторизации
     */
    public AuthPage toAuthPage(){
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(buttonLoginAndRegistration));
        buttonLoginAndRegistration.click();
        return new AuthPage(driver);
    }

    /**
     * Переход на страницу "Правовая база"
     * @return - страница "Правовая база"
     */
    public LawBasePage toLawBasePage(){
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(buttonLawBase));
        buttonLawBase.click();
        return new LawBasePage(driver);
    }
}
