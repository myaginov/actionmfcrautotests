package AccountantWebTest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Класс для страницы авторизации
 */
public class AuthPage {

    private WebDriver driver;

    @FindBy(id = "wf-enter")
    private WebElement linkCustomerEnter;

    @FindBy(id = "customer-enter")
    private WebElement buttonLogin;

    @FindBy(id = "email")
    private WebElement inputLogin;

    @FindBy(id = "password")
    private WebElement inputPassword;

    private String pageTitle = "Вход в Систему Главбух";

    private MainPage clickLogin(){
        buttonLogin.click();
        return new MainPage(driver);
    }

    private void setLoginAndPassword(String userLogin,String userPassword){
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(inputLogin));
        inputLogin.sendKeys(userLogin);
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(inputPassword));
        inputPassword.sendKeys(userPassword);
    }

    private void toLinkCustomerEnter(){
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(linkCustomerEnter));
        linkCustomerEnter.click();
    }

    public AuthPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        new WebDriverWait(driver, 10).until(ExpectedConditions.titleIs(pageTitle));
    }

    /**
     * Авторизация
     * @param userLogin - логин
     * @param userPassword - пароль
     * @return - главная страница с авторизованным пользователем
     */
    public MainPage login(String userLogin,String userPassword){
        toLinkCustomerEnter();
        setLoginAndPassword(userLogin, userPassword);
        return clickLogin();
    }
}
